import csv
import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/");
mydb = myclient["GlobalLandTemp"]
mycol = mydb["Temperature"]

with open('GolobalLandTemperatures.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')

    thislist = []

    for row in readCSV:
        thisdict = {
            "dt" : row[0],
            "LandAverageTemperature" : row[1],
            "LandMaxTemperature" : row[2],
            "LandMaxTemperatureUncertainty" : row[3],
            "LandMinTemperature" : row[4],
            "LandMinTemperatureUncertainty" : row[5],
            "LandAndOceanAverageTemperature" : row[6],
            "LandAndOceanAverageTemperatureUncertainty" : row[7]
        }

        thislist.append(thisdict)
        mycol.insert_one(thisdict)
        
